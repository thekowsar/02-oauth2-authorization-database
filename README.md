# Spring Boot 2 and OAuth2 integration (Password Grand): complete guide
In this project we present how to improve your SpringBoot app's security by using OAuth2 authentication.

## Building the project
You can use gradle wrapper which is configured in the project.

```
gradle clean build
```

## Running the application
You can use your IDE or command-line.

### IDE
Just run as Java application.

* authorization server
* resources server

### Command-line
You can use gradle for it.

```
cd build/libs
nohup java -jar jar_name.jar
```

## Available SpringBoot profiles
There are two:
* dev - for local development, uses H2 in memory database, enabled by default
* test - uses PostgreSQL database


**Curl to get access token** 

```
curl --location --request POST 'localhost:8080/oauth/token' \
--header 'Authorization: Basic Y2xpZW50LXdlYi1pZDpjbGllbnQtd2ViLXNlY3JldA==' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=mohammed.kowsar.rahman' \
--data-urlencode 'password=kowsar' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'scope=any'
```

You should receive an authentication token in a response. The example below.

```
{
    "access_token": "6e238874-75c3-47d9-a546-3f116485a43f",
    "token_type": "bearer",
    "refresh_token": "a9cc15f3-5608-4981-bcd4-aeb0155cbf0f",
    "expires_in": 41909,
    "scope": "any"
}
```

**Curl to get resource (test) by access token** 

```
curl --location --request GET 'http://localhost:9090/test' \
--header 'Authorization: Bearer 6e238874-75c3-47d9-a546-3f116485a43f' \
--data-raw ''
```

You should receive a response.

`Hello World`

**Curl to get resource (whoami) by access token** 

```
curl --location --request GET 'http://localhost:9090/whoami' \
--header 'Authorization: Bearer 6e238874-75c3-47d9-a546-3f116485a43f' \
--data-raw ''
```

You should receive your username in response.

`mohammed.kowsar.rahman`
