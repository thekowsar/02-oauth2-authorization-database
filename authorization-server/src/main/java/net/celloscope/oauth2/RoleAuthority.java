package net.celloscope.oauth2;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="roleauthority")
public class RoleAuthority {

    @Id
    @Column(name = "roleauthorityoid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleAuthorityOid;

    @Column(name="authorityid")
    private String authorityId;

    @Column(name="roleid")
    private String roleId;

}
