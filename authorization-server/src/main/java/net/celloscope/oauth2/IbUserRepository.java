package net.celloscope.oauth2;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IbUserRepository extends JpaRepository<IbUserEntity, String> {

    @NotFound(action = NotFoundAction.IGNORE)
    IbUserEntity findByUserId(String userId);


}
